package com.company.map;

public class Main {
    public static void main (String[] args) {
        MyTreeMap<Integer, Integer> map = new MyTreeMap<Integer, Integer>();

        map.put(10, 10);
        map.put(15, 15);
        map.put(5, 5);
        map.put(14, 14);
        map.put(16, 16);
        map.put(7, 7);
        map.put(3, 3);

        map.remove(10);
        map.remove(3);
        map.remove(14);
        map.put(10, 10); // return back

        map.values(); // show it
    }
}
