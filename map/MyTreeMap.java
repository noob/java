package com.company.map;

import java.util.Comparator;
import java.util.Random;

public class MyTreeMap<K,V> {
    public void values() {
        values(mRoot);
    }

    private void values(Node<K,V> node) {
        if (node != null) {
            if (node.mLeft != null) {
                values(node.mLeft);
            }

            if (node.mRight != null) {
                values(node.mRight);
            }

            System.out.println(node.mValue);
        }
    }

    private void genValues(Object[] p, Node<K,V> node, Integer i) {
        if (node != null) {
            if (node.mLeft != null) {
                genValues(p, node.mLeft, i);
            }

            if (node.mRight != null) {
                genValues(p, node.mRight, i);
            }

            p[i] = node.mValue;
            ++i;
        }
    }

    private int mSize;
    private final Comparator<? super K> mComparator;
    private Node<K,V> mRoot;

    private int compare(Object k1, Object k2) {
        return mComparator==null ? ((Comparable<? super K>)k1).compareTo((K)k2)
            : mComparator.compare((K)k1, (K)k2);
    }

    public MyTreeMap() {mComparator = null;}
    public MyTreeMap(Comparator<? super K> comparator) {mComparator = comparator;}

    private static class Node<K,V> {
        Node<K,V> mLeft;
        Node<K,V> mRight;
        Node<K,V> mUp;
        K mKey;
        V mValue;

        Node(Node<K,V> left, Node<K,V> right, Node<K,V> up, K key, V value) {
            mLeft = left;
            mRight = right;
            mUp = up;
            mKey = key;
            mValue = value;
        }
    }

    public boolean put (K key, V value) {
        boolean back = false;

        if(mRoot == null) {
            mRoot = new Node<K,V>(null, null, null, key, value);
            back = true;
            ++mSize;
        } else {
            NodeSearcher<K,V> searcher = getNode(key);
            Node<K, V> node = searcher.node;

            if(node == null) { // no key found
                Node<K, V> newNode = new Node(null, null, searcher.parent, key, value); // mBuf != null (ok)
                ++mSize;
                back = true;

                if (compare(key, searcher.parent.mKey) > 0) {
                    searcher.parent.mRight = newNode;
                } else { // result < 0
                    searcher.parent.mLeft = newNode;
                } // == 0 impossible, because - in this case node != null
            } else {
                node.mValue = value; // replace ?
            }
        }

        return back;
    }

    private static class NodeSearcher<K,V> {
        Node<K,V> node = null;
        Node<K,V> parent = null;
    }

    private NodeSearcher<K,V> getNode(K key) {
        NodeSearcher<K,V> back = new NodeSearcher<K,V>();
        Node<K, V> r = mRoot;
        int result;
        back.parent = r;

        while (r != null) {
            result = compare(key, r.mKey);

            if (result == 0) {
                back.node = r;

                break;
            }

            back.parent = r;

            if (result > 0) {
                r = r.mRight;
            } else { // result < 0
                r = r.mLeft;
            }
        }

        return back;
    }

    public V get(K key) {
        Node<K,V> node = getNode(key).node;

        return ((node != null) ? (node.mValue) : null);
    }

    private void rebuildTree_r(Node<K,V> node) {
        if (node != null) {
            if (node.mLeft != null) {rebuildTree_r(node.mLeft);}
            if (node.mRight != null) {rebuildTree_r(node.mRight);}

            put(node.mKey, node.mValue);

            node.mKey = null;
            node.mValue = null;
            node.mUp = null;
            node.mRight = null;
            node.mLeft = null;
        }
    }

    public V remove(K key) {
        V back;
        NodeSearcher<K,V> searcher = getNode(key);
        Node<K,V> node = searcher.node;

        if(node != null) {
            --mSize;
            back = node.mValue;

            /*
                NO REFERENCES (to references ) :((( java -= 1
            */

            if(node == mRoot) { // +
                if((node.mLeft != null) && (node.mRight != null)) {
                    if(new Random().nextBoolean()) {
                        node.mLeft.mUp = null;
                        mRoot = node.mLeft;
                        rebuildTree_r(node.mRight);
                    } else {
                        node.mRight.mUp = null;
                        mRoot = node.mRight;
                        rebuildTree_r(node.mLeft);
                    }
                } else if((node.mLeft != null)) {
                    node.mLeft.mUp = null;
                    mRoot = node.mLeft;
                } else if((node.mRight != null)) {
                    node.mRight.mUp = null;
                    mRoot = node.mRight;
                } else {
                    mRoot = null;
                }
            } else {
                if(searcher.parent.mLeft == node) {
                    if((node.mLeft != null) && (node.mRight != null)) {
                        node.mRight.mUp = searcher.parent;
                        searcher.parent.mLeft = node.mRight;
                        rebuildTree_r(node.mLeft);
                    } else if((node.mLeft != null)) {
                        node.mLeft.mUp = searcher.parent;
                        searcher.parent.mLeft = node.mLeft;
                    } else if((node.mRight != null)) {
                        node.mRight.mUp = searcher.parent;
                        searcher.parent.mLeft = node.mRight;
                    } else {
                        searcher.parent.mLeft = null;
                    }
                } else if (searcher.parent.mRight == node) {
                    if((node.mLeft != null) && (node.mRight != null)) {
                        node.mRight.mUp = searcher.parent;
                        searcher.parent.mRight = node.mRight;
                        rebuildTree_r(node.mLeft);
                    } else if((node.mLeft != null)) {
                        node.mLeft.mUp = searcher.parent;
                        searcher.parent.mRight = node.mLeft;
                    } else if((node.mRight != null)) {
                        node.mRight.mUp = searcher.parent;
                        searcher.parent.mRight = node.mRight;
                    } else {
                        searcher.parent.mRight = null;
                    }
                } else { // err
                    // upper node hasn't link to removing node
                }
            }
        } else {
            back = null;
        }

        return back;
    }
}