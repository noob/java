package com.company.students;

import com.company.students.Filter;

import java.util.List;

interface DataBase<T> {
    public boolean addData(T data);
    public boolean removeData(T data);
    public List<T> findData(Filter<T> filter);
}
