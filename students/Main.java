package com.company.students;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import static java.lang.Integer.parseInt;

public class Main {
    private static final BufferedReader mReader = new BufferedReader(new InputStreamReader(System.in));
    private static final InstituteDataBase mBase = new InstituteDataBase();
    private static List<Student> mBuf = null;
    private static boolean mRemove = false;

    private static List<Student> getResult(Filter<Student> filter) {
        return (mRemove ? mBase.findData(mBuf, filter) : mBase.findData(filter));
    }

    private static String[] interview (final String[] content) throws IOException {
        String[] back = new String[content.length];

        for(int i = 0; i < content.length; ++i) {
            System.out.println(content[i]);
            back[i] = mReader.readLine();
        }

        return back;
    }

    private static List<Student> findStudents() throws IOException {
        List<Student> back = null;

        System.out.println(
            "Поиск\n\n" +
            "1.ФИО\n" +
            "2.Год|Месяц|День (рождения)\n" +
            "3.Факультет|Группа\n\n" +
            "4.Главное меню"
        );

        switch(parseInt(mReader.readLine())) {
            case 1: {
                final String[] content = new String[] {
                    "\nФамилия:",
                    "Имя:",
                    "Отчество:"
                };

                final String[] unique = interview (content);

                back = getResult(new Filter<Student>() {
                    @Override
                    public boolean isContains(Student obj) {
                        return (
                            (unique[0].equals(obj.firstName)) &&
                                (unique[1].equals(obj.secondName)) &&
                                (unique[2].equals(obj.middleName))
                        );
                    }
                });

                break;
            } case 2: {
                final String[] content = new String[] {
                    "\nГод:",
                    "Месяц:",
                    "День:"
                };

                final String[] date = interview (content);

                back = getResult(new Filter<Student>() {
                    @Override
                    public boolean isContains(Student obj) {
                        return (
                            (date[0].equals(obj.bornYear)) &&
                                (date[1].equals(obj.bornMonth)) &&
                                (date[2].equals(obj.bornDay))
                        );
                    }
                });

                break;
            } case 3: {
                final String[] content = new String[] {
                    "\nФакультет:",
                    "Группа:"
                };

                final String[] info = interview (content);

                back = getResult(new Filter<Student>() {
                    @Override
                    public boolean isContains(Student obj) {
                        return (
                            (info[0].equals(obj.faculty)) &&
                                (info[1].equals(obj.group))
                        );
                    }
                });

                break;
            } case 4: { // exit
                break;
            } default: {
                break;
            }
        }

        return back;
    }

    private static Student removeStudent() throws IOException {
        System.out.println("\nИспользуем поиск для удаления:");
        mRemove = false;
        List<Student> list = findStudents();
        mRemove = true;
        Student back = null;

        while(true) {
            if(list != null) {
                if(list.size() > 1) {
                    // check for cycled search ?
                    System.out.println("\nУточните критерии поиска (найдено " + list.size() + ")");
                    mBuf = list;
                    list = findStudents();
                } else {
                    back = list.get(0);

                    if(mBase.removeData(back)) {
                        System.out.println("\nИсключен:\n" + back);
                    } else {
                        System.out.println("\nСовпадений не найдено:\n");
                    }

                    break;
                }
            } else {
                break;
            }
        }

        return back;
    }

    private static void newStudent() throws IOException {
        final String data[] = new String[] {
            "Фамилия:",
            "Имя:",
            "Отчество:",
            "Год:",
            "Месяц:",
            "Число:",
            "Факультет:",
            "Группа:"
        };

        String result[] = new String[data.length];

        for(int i = 0;i < data.length; ++i) {
            System.out.println(data[i]);
            result[i] = mReader.readLine();
        }

        Student student = new Student (
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7]
        );

        mBase.addData(student);
        System.out.println("\nДобавлен(а):\n" + student);
    }

    public static void main (String[] args) throws IOException, ClassNotFoundException {
        // mBase.readCfg(); // test

        int i;

        goto_finish:
        while (true) {
            System.out.println (
                "Главное меню\n\n" +
                "1.Создать нового студента\n" +
                "2.Исключить студента\n" +
                "3.Найти студент(а/ов)\n" +
                "\n" +
                "4.Выход"
            );

            i = parseInt(mReader.readLine());

            switch(i) {
                case 1: {
                    newStudent();

                    break;
                } case 2: {
                    if (mBase.isEmpty()) {
                        System.out.println("\nНекого исключать (база пуста)");
                    } else {
                        mRemove = true;
                        removeStudent();
                    }

                    break;
                } case 3: {
                    if (!mBase.isEmpty()) {
                        mRemove = false;
                        List<Student> result = findStudents();

                        if ((result == null) || (result.size() == 0)) {
                            System.out.println("\nСовпадений не найдено");
                        } else {
                            for (Student x : new ArrayList<Student>(new TreeSet(result))) {
                                System.out.println(x);
                            }
                        }
                    } else {
                        System.out.println("\nПоиск невозможен (база пуста)");
                    }

                    break;
                } case 4: {
                    break goto_finish;
                } default: {
                    break;
                }
            }
        }

        // mBase.writeCfg(); // test
    }
}