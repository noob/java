package com.company.students;

import java.io.Serializable;

public class Student
    implements Serializable,
    Comparable < Student > {

    public final String firstName;
    public final String secondName;
    public final String middleName;
    public final String group;
    public final String faculty;
    public final String bornDay;
    public final String bornMonth;
    public final String bornYear;

    public Student (
        String firstName,
        String secondName,
        String middleName,
        String bornYear,
        String bornMonth,
        String bornDay,
        String faculty,
        String group
    ) throws NullPointerException {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.bornDay = bornDay;
        this.bornYear = bornYear;
        this.bornMonth = bornMonth;
        this.group = group;
        this.faculty = faculty;

        if (
            firstName == null ||
            secondName == null ||
            middleName == null ||
            bornDay == null ||
            bornYear == null ||
            bornMonth == null ||
            group == null ||
            faculty == null
        ) {
            throw new NullPointerException();
        }
    }

    @Override
    public int compareTo(Student other){
        int back = firstName.compareTo(other.firstName);

        if(back == 0) {
            back = secondName.compareTo(other.secondName);

            if(back == 0) {
                back = middleName.compareTo(other.middleName);
            }
        }

        return back;
    }

    @Override
    public String toString() {
        return
            "Student{" +
             "firstName='" + firstName + '\'' +
             ", secondName='" + secondName + '\'' +
             ", middleName='" + middleName + '\'' +
             ", group='" + group + '\'' +
             ", faculty='" + faculty + '\'' +
             ", bornDay='" + bornDay + '\'' +
             ", bornMonth='" + bornMonth + '\'' +
             ", bornYear='" + bornYear + '\'' +
             '}';
    }

    @Override
    public boolean equals(Object o) {
        boolean back = false;

        if (this == o) {
            back = true;
        } else {
            if(((o == null) || (getClass() != o.getClass())) == false) {
                Student x = (Student) o;

                back =
                    bornDay.equals(x.bornDay) &&
                    bornMonth.equals(x.bornMonth) &&
                    bornYear.equals(x.bornYear) &&
                    faculty.equals(x.faculty) &&
                    firstName.equals(x.firstName) &&
                    group.equals(x.group) &&
                    middleName.equals(x.middleName) &&
                    secondName.equals(x.secondName);
            }
        }

        return back;
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();

        result = 31 * result + secondName.hashCode();
        result = 31 * result + middleName.hashCode();
//        result = 31 * result + group.hashCode();
//        result = 31 * result + faculty.hashCode();
//        result = 31 * result + bornDay.hashCode();
//        result = 31 * result + bornMonth.hashCode();
//        result = 31 * result + bornYear.hashCode();

        return result;
    }
}