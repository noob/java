package com.company.students;

interface Filter<T> {
    public boolean isContains(T obj);
}
