package com.company.students;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class InstituteDataBase implements DataBase<Student>, Serializable {
    private final List<Student> mInside;

    public void writeCfg() throws IOException {
        ObjectOutputStream os =
            new ObjectOutputStream(new FileOutputStream(new File("institute_database.x")));

        os.writeObject(this);
        os.close();
    }

    public static void readCfg() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("institute_database.x")));
        InstituteDataBase base = (InstituteDataBase)ois.readObject();

        for(Student x : base.mInside) {
            System.out.println(x);
        }

        ois.close();
    }

    public boolean isEmpty() {
        return mInside.isEmpty();
    }

    public InstituteDataBase() {
        mInside = new LinkedList<Student>();
    }

    @Override
    public boolean addData(Student data) {
        boolean back = false;

        if(data != null) {
            back = true;

            for (Student s : mInside) {
                if (data.equals(s)) {
                    back = false;

                    break;
                }
            }

            if(back == true) {
                mInside.add(data);
            }
        }

        return back;
    }

    @Override
    public boolean removeData(Student data) {
        boolean back = false;

        if(data != null) {
            for (Student s : mInside) {
                if (data.equals(s)) {
                    back = true;
                    mInside.remove(s);

                    break;
                }
            }
        }

        return back;
    }

    public static List<Student> findData(List<Student> list, Filter<Student> filter) {
        List<Student> back = new LinkedList<Student>();

        for(Student s : list) {
            if(filter.isContains(s)) {
                back.add(s);
            }
        }

        return back;
    }

    @Override
    public List<Student> findData(Filter<Student> filter) {
        List<Student> back = new LinkedList<Student>();

        for(Student s : mInside) {
            if(filter.isContains(s)) {
                back.add(s);
            }
        }

        return back;
    }
}
