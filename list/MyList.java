package com.company.list;

public interface MyList<T> {
    public int size();
    default public boolean isEmpty() {return (size() == 0);};
    public boolean add(T obj);
    public boolean remove(T obj);
    public T get(int index);
}
