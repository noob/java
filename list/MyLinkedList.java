package com.company.list;

import java.util.Iterator;

public class MyLinkedList<T>
    implements MyList<T>,
    Iterable<T> {

    private Node<T> mFirst;
    private Node<T> mLast;
    private int mSize;

    private static class Node<T> {
        Node<T> mPre;
        Node<T> mNext;
        T mElem;

        Node(Node<T> pre, Node<T> next, T elem) {
            mPre = pre;
            mNext = next;
            mElem = elem;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new InsideIterator();
    }

    private class InsideIterator implements Iterator<T> {
        private Node<T> mCur = mFirst;

        public boolean hasNext() {
            return (mCur != null);
        }

        public T next() {
            T back = mCur.mElem;
            mCur = mCur.mNext;

            return back;
        }
    }

    @Override
    public T get(int index) {
        Node<T> node = getNode(index);
        T back = null;

        if(node != null) {
            back = node.mElem;
        }

        return back;
    }

    @Override
    public int size() {return mSize;}

    private Node<T> getNode(int index) {
        int i;
        Node<T> back;

        if (index < (mSize / 2)) {
            back = mFirst;

            for(i = 0; i < index; ++i) {
                back = back.mNext;
            }
        } else {
            back = mLast;

            for(i = (mSize - 1); i > index; --i) {
                back = back.mPre;
            }
        }

        return back;
    }

    @Override
    public boolean add(T obj) {
        final Node<T> newNode = new Node<>(mLast, null, obj);

        if(mLast == null) {
            if(mFirst == null) {
                mFirst = newNode;
            } else {
                mLast = newNode;
                mLast.mPre = mFirst;
                mFirst.mNext = mLast;
            }
        } else {
            mLast.mNext = newNode;
            mLast = newNode;
        }

        ++mSize;

        return true;
    }

    private T unlink(Node<T> node) {
        if(node == null) {
            throw new NullPointerException();
        }

        if(node == mFirst) {
            if(mLast == null) {
                mFirst = null;
            } else {
                mFirst.mNext.mPre = null;
                mFirst = mFirst.mNext;

                if(mFirst == mLast) {
                    mLast = null;
                }
            }
        } else if(node == mLast) {
            mLast.mPre.mNext = null;
            mLast = mLast.mPre;

            if(mFirst == mLast) {
                mLast = null;
            }
        } else {
            node.mNext.mPre = node.mPre;
            node.mPre.mNext = node.mNext;
        }

        --mSize;

        return node.mElem;
    }

    @Override
    public boolean remove(T obj) {
        boolean back = false;

        if (obj == null) {
            for (Node<T> x = mFirst; x != null; x = x.mNext) {
                if (x.mElem == null) {
                    unlink(x);
                    back = true;

                    break;
                }
            }
        } else {
            for (Node<T> x = mFirst; x != null; x = x.mNext) {
                if (obj.equals(x.mElem)) {
                    unlink(x);
                    back = true;

                    break;
                }
            }
        }

        return back;
    }
}
