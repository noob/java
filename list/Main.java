package com.company.list;

public class Main {
    public static void main(String[] args) {
        MyLinkedList<Integer> list = new MyLinkedList<>();

        list.add(1);
        list.add(2);
        list.add(3);

        for(Integer i : list) {System.out.print(i);} // out 123
        System.out.print("\n");

        list.remove(2);

        for(Integer i : list) {System.out.print(i);} // out 13 (remove from middle)
        System.out.print("\n");

        list.add(2);

        for(Integer i : list) {System.out.print(i);} // out 132
        System.out.print("\n");

        list.remove(2);

        for(Integer i : list) {System.out.print(i);} // out 13 (remove from end)
        System.out.print("\n");

        list.add(4);

        for(Integer i : list) {System.out.print(i);} // out 134
        System.out.print("\n");

        list.remove(1);

        for(Integer i : list) {System.out.print(i);} // out 34 (remove from begin)
        System.out.print("\n");
    }
}
