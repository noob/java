package com.company;

import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    public static void main(String[] args) throws Exception {
        MyBuffer<String> buf = new MyBuffer<String>();

        AtomicBoolean state =
            new AtomicBoolean(false);

        MyReader myR = new MyReader(buf, state);
        MyWriter myW = new MyWriter(buf, state);
        Thread trRead = new Thread(myR);
        Thread trWrite = new Thread(myW);

        trRead.start();
        trWrite.start();
        trRead.join();
        trWrite.join();

        System.err.println("Done! :)");
    }
}
