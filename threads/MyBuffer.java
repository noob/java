package com.company;

/*
    few container versions ..
*/

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// third version
//public class MyBuffer<T> {
//    private static final int MAX_SIZE = 1024;
//
//    private final LinkedBlockingQueue<T> mBuf =
//        new LinkedBlockingQueue<T>(MAX_SIZE);
//
//    public T pop () {
//        T back = null;
//
//        try {
//            back = mBuf.take();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        return back;
//    }
//
//    public int size() {
//        return mBuf.size();
//    }
//
//    public void put(T param) {
//        try {
//            mBuf.put(param);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//}

//second version ???
public class MyBuffer<T> {
    private static final int MAX_SIZE = 1024;
    private final List<T> mBuf = new LinkedList<T>();

    ReentrantLock lock = new ReentrantLock();
	Condition notFull = lock.newCondition();
	Condition notEmpty = lock.newCondition();

    public int size() {
        lock.lock();
        int back = mBuf.size();
        lock.unlock();

        return back;
    }

    public void put(T e) {
        lock.lock();

		while(mBuf.size() >= MAX_SIZE) {
		    try {
			    notFull.await();
			} catch (InterruptedException exception) {
                exception.printStackTrace();
			}
		}

        mBuf.add(e);
		notEmpty.signal();
		lock.unlock();
    }

    public T pop() {
        lock.lock();

		while(mBuf.size() == 0) {
		    try {
			    notEmpty.await();
			} catch (InterruptedException e) {
                e.printStackTrace();
			}
		}

        T back = mBuf.remove(0);

		notFull.signal();
		lock.unlock();

        return back;
    }
}

// first version
//public class MyBuffer<T> {
//    private static final int MAX_SIZE = 1024;
//    private final Object mSync = new Object();
//    private final List<T> mBuf = new LinkedList<T>();
//
//    public int size() {
//        synchronized (mSync) {
//            return mBuf.size();
//        }
//    }
//
//    public void put(T e) {
//        synchronized (mSync) {
//            while(mBuf.size() >= MAX_SIZE) {
//                try {
//                    mSync.wait();
//                } catch (InterruptedException exception) {
//                    exception.printStackTrace();
//                }
//            }
//
//            mBuf.add(e);
//            mSync.notifyAll();
//        }
//    }
//
//    public T pop() {
//        synchronized (mSync) {
//            while(mBuf.size() == 0) {
//                try {
//                    mSync.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            T back = mBuf.remove(0);
//            mSync.notifyAll();
//
//            return back;
//        }
//    }
//}
