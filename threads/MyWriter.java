package com.company;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

class MyWriter implements Runnable {
    private final MyBuffer<String> mBuf;
    private final AtomicBoolean mState;
    private final List<MyFilter> mFilters;
    private static final BufferedWriter mWriter;
    private static boolean mClosed = false;

    static {
        FileWriter fL = null;

        try {
            fL = new FileWriter("out.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(fL != null) {
            mWriter = new BufferedWriter(fL);
        } else {
            mWriter = null;
        }
    }

    public static void closeHandle() throws IOException {
        if(mWriter != null) {
            synchronized (mWriter) {
                if (mClosed == false) {
                    mClosed = true;
                    mWriter.close();
                }
            }
        }
    }

    MyWriter(MyBuffer<String> mBuf, AtomicBoolean state, MyFilter ... filters) {
        this.mBuf = mBuf;
        this.mState = state;

        if((filters != null) && (filters.length != 0)) { // != null ? o_O
            this.mFilters = new ArrayList<MyFilter>(Arrays.asList(filters));
        } else {
            this.mFilters = new ArrayList<MyFilter>();
            this.mFilters.add(new FilterStringLen());
        }
    }

    @Override
    public void run() {
        String sBuf;

        while(true) {
            if(mState.get()) {
                mState.set(false);

                break;
            }

            System.err.println("remove");
            sBuf = mBuf.pop();

            if(sBuf != null) {
                try {
                    mWriter.write(sBuf + '\n');
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}