package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

class MyReader implements Runnable {
    private final MyBuffer<String> mBuf;
    private final AtomicBoolean mState;

    MyReader(MyBuffer<String> mBuf, AtomicBoolean state) {
        this.mBuf = mBuf;
        this.mState = state;
    }

    @Override
    public void run() {
        try {
            FileReader fileReader = new FileReader("book1.txt");
            BufferedReader mReader;

            mReader = new BufferedReader(fileReader);
            String buf;
            String[] line;

            try {
                while (true) {
                    try {
                        buf = mReader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();

                        break;
                    }

                    if (buf == null) {
                        mState.set(true);

                        break;
                    }

                    line = buf.split(" ");

                    for (String s : line) {
                        System.err.println("add");
                        mBuf.put(s);
                    }
                }
            } finally {
                try {
                    mReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}